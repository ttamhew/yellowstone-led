#include <Arduino.h>

#include <FadeLed.h>
#include <Ethernet.h>
#include <SD.h>

#define RED_LED_PIN 6
#define BLUE_LED_PIN 5
#define GREEN_LED_PIN 9

#define LED_FADE_TIME 1500
#define BUTTON_PIN 2

FadeLed RedLed(RED_LED_PIN);
FadeLed BlueLed(BLUE_LED_PIN);
FadeLed GreenLed(GREEN_LED_PIN);

int buttonState = 0; // button status variable

void setup() {
    RedLed.setTime(LED_FADE_TIME, true);
    GreenLed.setTime(LED_FADE_TIME, true);
    BlueLed.setTime(LED_FADE_TIME, true);

    pinMode(BUTTON_PIN, INPUT);
}

void loop() {
    loopUpdateState





    if (buttonState == HIGH) {
        // turn LED on:
        RedLed.set(255);
        BlueLed.set(255);
        GreenLed.set(255);
    } else {
        // turn LED off:
        RedLed.set(0);
        BlueLed.set(0);
        GreenLed.set(0);
    }
}

// Update
void loopUpdateState {
        FadeLed::update(); //updates all FadeLed objects

        if (isButtonPressed()) {
            buttonState = !buttonState;
            delay(250);
        }
}

int isButtonPressed(){
    return (digitalRead(BUTTON_PIN) == HIGH);
}

