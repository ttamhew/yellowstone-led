# yellowstone-led
Yellowstone LED is a Arduino Uno project for controlling a RGB LED strip via your local netowrk. Yellowstone LED can be controlled via a basic API or its own control page.

![Photo Yellostone LED sat on desk, illuminated](docs/img/img1.jpg "Yellowstone LED (Revsion 1")
![Photo Yellostone LED PCB scematic](docs/img/pcb1.png "Yellowstone LED PCB schematic (Revsion 2")


## Requirments
The following are required to start using the yellowstone-led controller:
1. Arduino Uno
2. Compatible ethernet sheild
3. Micro SD card
4. 12v Power supply - power supplied via VIN & GRN pins
5. RGB Led Strip
6. Laptop/PC with Arduino IDE and compatible usb cable to upload the sketch

To build the controller you will need the components listed below. Or order them with your PCB via aisler.net (see links below). 
1. Order a yellowstone-led PCB (see links below)
2. 3x 10K Ohm resistors
3. 3x Mofset
4. 1 x 2-pin PCB Terminal Block, 3.5mm Pitch 
5. to be completed

## Libaries
[FadeLED](https://github.com/septillion-git/FadeLed)

## Current version
PCB: [revision 5](https://aisler.net/yellowstone/yellowstone/yellowstone-led?timetravel=600089) 

## Order your PCB
Order our yellowstone-led PCB from [aisler.net](https://aisler.net/yellowstone/yellowstone/yellowstone-led)

## Gallery
![Photo Yellostone LED](docs/img/img2.jpg "Yellowstone LED (Revsion 1")
![Photo Yellostone LED](docs/img/img3.jpg "Yellowstone LED (Revsion 1")
![Photo Yellostone LED](docs/img/img4.jpg "Yellowstone LED (Revsion 1")
